# argp

Dead simple argument parsing for C.

Functions nearly identically to the [plan9](https://github.com/0intro/plan9/blob/0aab9506bfc2acb5203a411b917cea62cf3eeb91/sys/src/cmd/tcs/plan9.h#L10)
or [suckless/20h](https://git.suckless.org/st/file/arg.h.html) implementations, just with ARGNEXT instead of ARGF/EARGF, 'progname' instead of argv0, and it's a bit easier to read (to me).

## Usage

The example.c file in this repository shows how it can be used for flag arguments and options.

## Drawbacks

Like the plan9 and 20h implementations, argp cannot parse "long" arguments that span multiple characters.
