#include <stdio.h>
#include <stdlib.h>
#include "argp.h"

void
usage()
{
    printf("%s [-a] [-b string] [arg ...]\n", progname);
    exit(1);
}

int
main(int argc, char **argv)
{
    char *bopt = "a default value";
    int aflag, i;

    aflag = 0;

    ARGBEGIN {
        case 'a':
            aflag++;
            break;
        case 'b':
            bopt = ARGNEXT(usage());
            break;
        default:
            usage();
            break;
    } ARGEND

    printf("Program name: %s\na flag: %d\nb option: %s\nExtra: ", progname, aflag, bopt);
    for (i = 0; i < argc; i++)
        printf("%s ", argv[i]);
    printf("\n");
}
